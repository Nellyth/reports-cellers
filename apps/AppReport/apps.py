from django.apps import AppConfig


class AppreportConfig(AppConfig):
    name = 'AppReport'
