from import_export import resources
from .models import Empresa, Empleado, Cargo, Bodega, Turno, Ubicacion, Jornada, Festivos, Concepto, Novedad


class EmpresaResource(resources.ModelResource):
    class Meta:
        model = Empresa


class EmpleadoResource(resources.ModelResource):
    class Meta:
        model = Empleado


class CargoResource(resources.ModelResource):
    class Meta:
        model = Cargo


class BodegaResource(resources.ModelResource):
    class Meta:
        model = Bodega


class TurnoResource(resources.ModelResource):
    class Meta:
        model = Turno


class UbicacionResource(resources.ModelResource):
    class Meta:
        model = Ubicacion


class JornadaResource(resources.ModelResource):
    class Meta:
        model = Jornada


class FestivosResource(resources.ModelResource):
    class Meta:
        model = Festivos


class ConceptoResource(resources.ModelResource):
    class Meta:
        model = Concepto


class NovedadResource(resources.ModelResource):
    class Meta:
        model = Novedad