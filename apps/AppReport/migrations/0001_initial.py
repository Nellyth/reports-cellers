# Generated by Django 2.2.1 on 2019-05-30 20:59

from django.db import migrations, models
import django.db.models.deletion
import multiselectfield.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=45, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Concepto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=35, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('apellido', models.CharField(max_length=50)),
                ('cedula', models.CharField(max_length=15, unique=True)),
                ('carne', models.CharField(max_length=20, unique=True)),
                ('cargo', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='AppReport.Cargo')),
            ],
        ),
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=45, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Festivos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateTimeField()),
                ('fecha_final', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Novedad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('decripcion', models.TextField()),
                ('fecha_inicio', models.DateTimeField()),
                ('fecha_final', models.DateTimeField()),
                ('concepto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Concepto')),
                ('empleado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Empleado')),
            ],
        ),
        migrations.CreateModel(
            name='Ubicacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=25, unique=True)),
                ('descripcion', models.TextField()),
                ('ubicacion_padre', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='AppReport.Ubicacion')),
            ],
        ),
        migrations.CreateModel(
            name='Turno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.TextField()),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField(null=True)),
                ('hora_inicio', models.TimeField()),
                ('hora_fin', models.TimeField()),
                ('dia', multiselectfield.db.fields.MultiSelectField(choices=[('Lunes', 'Lunes'), ('Martes', 'Martes'), ('Miercoles', 'Miercoles'), ('Jueves', 'Jueves'), ('Viernes', 'Viernes'), ('Sabado', 'Sabado'), ('Domingo', 'Domingo')], max_length=52, null=True)),
                ('festivos', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='AppReport.Festivos')),
                ('novedad', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='AppReport.Novedad')),
            ],
        ),
        migrations.CreateModel(
            name='Jornada',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('empleado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Empleado')),
                ('turno', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Turno')),
                ('ubicacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Ubicacion')),
            ],
        ),
        migrations.CreateModel(
            name='Bodega',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.TextField()),
                ('empresa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Empresa')),
                ('id_ubicacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AppReport.Ubicacion')),
            ],
        ),
    ]
