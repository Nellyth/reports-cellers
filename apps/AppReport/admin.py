from django.contrib import admin
from .models import Empleado, Cargo, Ubicacion, Turno, Empresa, Bodega, Concepto, Festivos, Jornada, Novedad

admin.site.register(Empleado)
admin.site.register(Cargo)
admin.site.register(Ubicacion)
admin.site.register(Turno)
admin.site.register(Empresa)
admin.site.register(Bodega)
admin.site.register(Concepto)
admin.site.register(Festivos)
admin.site.register(Jornada)
admin.site.register(Novedad)

