from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from apps.AppReport.views import DataImporterCreateViewEmpresa, DataImporterCreateViewEmpleado, DataImporterCreateViewCargo,\
    DataImporterCreateViewBodega, DataImporterCreateViewTurno, DataImporterCreateViewUbicacion, DataImporterCreateViewJornada,\
    DataImporterCreateViewFestivos, DataImporterCreateViewConcepto, DataImporterCreateViewNovedad, \
    exportEmpresa, exportEmpleado, exportCargo, exportBodega, exportTurno, exportUbicacion, exportJornada, \
    exportFestivos, exportConcepto, exportNovedad

urlpatterns = [
    path('data_importer-empresa/', DataImporterCreateViewEmpresa.as_view(), name='Data-Empresa'),
    path('data_importer-empleado/', DataImporterCreateViewEmpleado.as_view(), name='Data-Empresa'),
    path('data_importer-cargo/', DataImporterCreateViewCargo.as_view(), name='Data-Empresa'),
    path('data_importer-bodega/', DataImporterCreateViewBodega.as_view(), name='Data-Empresa'),
    path('data_importer-turno/', DataImporterCreateViewTurno.as_view(), name='Data-Empresa'),
    path('data_importer-ubicacion/', DataImporterCreateViewUbicacion.as_view(), name='Data-Empresa'),
    path('data_importer-jornada/', DataImporterCreateViewJornada.as_view(), name='Data-Empresa'),
    path('data_importer-festivos/', DataImporterCreateViewFestivos.as_view(), name='Data-Empresa'),
    path('data_importer-concepto/', DataImporterCreateViewConcepto.as_view(), name='Data-Empresa'),
    path('data_importer-novedad/', DataImporterCreateViewNovedad.as_view(), name='Data-Empresa'),
    url(r'^exportEmpresa/$', exportEmpresa, name='export_empresa'),
    url(r'^exportEmpleado/$', exportEmpleado, name='export_empleado'),
    url(r'^exportCargo/$', exportCargo, name='export_cargo'),
    url(r'^exportBodega/$', exportBodega, name='export_bodega'),
    url(r'^exportTurno/$', exportTurno, name='export_turno'),
    url(r'^exportUbicacion/$', exportUbicacion, name='export_ubicacion'),
    url(r'^exportJornada/$', exportJornada, name='export_jornada'),
    url(r'^exportFestivos/$', exportFestivos, name='export_festivos'),
    url(r'^exportConcepto/$', exportConcepto, name='export_concepto'),
    url(r'^exportNovedad/$', exportNovedad, name='export_novedad'),
]