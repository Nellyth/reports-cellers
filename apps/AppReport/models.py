from django.db import models
from multiselectfield import MultiSelectField


class Cargo(models.Model):
    nombre = models.CharField(max_length=45, unique=True, null=False)

    def __str__(self):
        return self.nombre


class Empleado(models.Model):
    nombre = models.CharField(max_length=50, null=False)
    apellido = models.CharField(max_length=50, null=False)
    cedula = models.CharField(max_length=15, unique=True, null=False)
    carne = models.CharField(max_length=20, unique=True, null=False)
    cargo = models.ForeignKey(Cargo, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Ubicacion(models.Model):
    nombre = models.CharField(max_length=25, null=False, unique=True)
    descripcion = models.TextField(null=False)
    ubicacion_padre = models.ForeignKey('Ubicacion', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "Nombre: {}, Padre: {}".format(self.nombre, self.ubicacion_padre if self.ubicacion_padre else 'No asignado' )


class Empresa(models.Model):
    nombre = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return self.nombre


class Bodega(models.Model):
    descripcion = models.TextField(null=False)
    id_ubicacion = models.ForeignKey(Ubicacion, null=False, on_delete=models.CASCADE)
    empresa = models.ForeignKey(Empresa, null=False, on_delete=models.CASCADE)


class Concepto(models.Model):
    nombre = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.nombre


class Novedad(models.Model):
    decripcion = models.TextField()
    fecha_inicio = models.DateTimeField(null=False)
    fecha_final = models.DateTimeField(null=False)
    concepto = models.ForeignKey(Concepto, null=False, on_delete=models.CASCADE)
    empleado = models.ForeignKey('Empleado', null=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.decripcion


class Festivos(models.Model):
    fecha_inicio = models.DateTimeField(null=False)
    fecha_final = models.DateTimeField(null=False)

    def __str__(self):
        return "Id: {}, Fecha_inicio: {}, Fecha_fin: {}".format(self.id, self.fecha_inicio,self.fecha_final)


class Turno(models.Model):
    choice_dia = (
        ('Lunes', 'Lunes'),
        ('Martes', 'Martes'),
        ('Miercoles', 'Miercoles'),
        ('Jueves', 'Jueves'),
        ('Viernes', 'Viernes'),
        ('Sabado', 'Sabado'),
        ('Domingo', 'Domingo')
    )
    descripcion = models.TextField(null=False)
    fecha_inicio = models.DateField(null=False)
    fecha_final = models.DateField(null=True)
    hora_inicio = models.TimeField(null=False)
    hora_fin = models.TimeField(null=False)
    dia = MultiSelectField(choices=choice_dia, null=True)
    novedad = models.ForeignKey('Novedad', null=True, on_delete=models.CASCADE)
    festivos = models.ForeignKey('Festivos', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.descripcion


class Jornada(models.Model):
    empleado = models.ForeignKey('Empleado', null=False, on_delete=models.CASCADE)
    turno = models.ForeignKey('Turno', null=False, on_delete=models.CASCADE)
    ubicacion = models.ForeignKey('Ubicacion', null=False, on_delete=models.CASCADE)