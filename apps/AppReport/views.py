from data_importer.views import DataImporterForm
from data_importer.importers import XLSXImporter

from .models import Empresa, Empleado, Cargo, Bodega, Turno, Ubicacion, Jornada, Festivos, Concepto, Novedad
from django.http import HttpResponse
from .resources import EmpresaResource, EmpleadoResource, CargoResource, BodegaResource, TurnoResource, UbicacionResource,\
    JornadaResource, FestivosResource, ConceptoResource, NovedadResource


class MyXLSXImporterModelEmpresa(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Empresa


class DataImporterCreateViewEmpresa(DataImporterForm):
    importer = MyXLSXImporterModelEmpresa
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Empresa',
        'template_file': '.exportEmpresa',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelEmpleado(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Empleado


class DataImporterCreateViewEmpleado(DataImporterForm):
    importer = MyXLSXImporterModelEmpleado
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Empleado',
        'template_file': 'exportEmpleado',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelCargo(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Cargo


class DataImporterCreateViewCargo(DataImporterForm):
    importer = MyXLSXImporterModelCargo
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Cargo',
        'template_file': 'exportCargo',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelBodega(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Bodega


class DataImporterCreateViewBodega(DataImporterForm):
    importer = MyXLSXImporterModelBodega
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Bodega',
        'template_file': 'exportBodega',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelTurno(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Turno


class DataImporterCreateViewTurno(DataImporterForm):
    importer = MyXLSXImporterModelTurno
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Turno',
        'template_file': 'exportTurno',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelUbicacion(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Ubicacion


class DataImporterCreateViewUbicacion(DataImporterForm):
    importer = MyXLSXImporterModelUbicacion
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Ubicacion',
        'template_file': 'exportUbicacion',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelJornada(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Jornada


class DataImporterCreateViewJornada(DataImporterForm):
    importer = MyXLSXImporterModelJornada
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Jornada',
        'template_file': 'exportJornada',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelFestivos(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Festivos


class DataImporterCreateViewFestivos(DataImporterForm):
    importer = MyXLSXImporterModelFestivos
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Festivos',
        'template_file': 'exportFestivos',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelConcepto(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Concepto


class DataImporterCreateViewConcepto(DataImporterForm):
    importer = MyXLSXImporterModelConcepto
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Concepto',
        'template_file': 'exportConcepto',
        'success_message': "File uploaded successfully"
    }


class MyXLSXImporterModelNovedad(XLSXImporter):
    class Meta:
        ignore_first_line = True
        model = Novedad


class DataImporterCreateViewNovedad(DataImporterForm):
    importer = MyXLSXImporterModelNovedad
    template_name = 'my_data_importer.html'
    extra_context = {
        'title': 'Novedad',
        'template_file': 'exportNovedad',
        'success_message': "File uploaded successfully"
    }


def exportEmpresa(request):
    dynamic_resource = EmpresaResource()
    return dynamic_export(request, dynamic_resource)


def exportEmpleado(request):
    dynamic_resource = EmpleadoResource()
    return dynamic_export(request, dynamic_resource)


def exportCargo(request):
    dynamic_resource = CargoResource()
    return dynamic_export(request, dynamic_resource)


def exportBodega(request):
    dynamic_resource = BodegaResource()
    return dynamic_export(request, dynamic_resource)


def exportTurno(request):
    dynamic_resource = TurnoResource()
    return dynamic_export(request, dynamic_resource)


def exportUbicacion(request):
    dynamic_resource = UbicacionResource()
    return dynamic_export(request, dynamic_resource)


def exportJornada(request):
    dynamic_resource = JornadaResource()
    return dynamic_export(request, dynamic_resource)


def exportFestivos(request):
    dynamic_resource = FestivosResource()
    return dynamic_export(request, dynamic_resource)


def exportConcepto(request):
    dynamic_resource = ConceptoResource()
    return dynamic_export(request, dynamic_resource)


def exportNovedad(request):
    dynamic_resource = NovedadResource()
    return dynamic_export(request, dynamic_resource)


def dynamic_export(request, dynamic_resource):
    data = dynamic_resource.Meta.model._meta.db_table.split('_')[1]
    dataset = dynamic_resource.export()
    response = HttpResponse(dataset.xlsx, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="{}.xlsx"'.format(data)
    return response